To explore reducing an image to outlines

[using this](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html)

[View the notebook](https://gitlab.com/HouseItMade/outlines_in_images/blob/master/applying_filters_to_images.ipynb)